"""Tests for the footer generation."""
from importlib import resources
import os
import pathlib
import tempfile
import unittest
from unittest import mock

import freezegun

from cki_lib import footer
from cki_lib import yaml

from . import assets


@freezegun.freeze_time('2021-01-01T00:00:00.0+00:00')
class TestFooter(unittest.TestCase):
    """Tests for the footer generation."""

    cases = (
        ('text', 'text_footer', 'footer/text.txt'),
        ('jira', 'jira_footer', 'footer/jira.txt'),
        ('gitlab', 'gitlab_footer', 'footer/gitlab.txt'),
    )

    config = (resources.files(assets) / 'footer/config.yml').read_text('utf8')

    def test_footer_context(self):
        """Check the footer with an explicit context."""
        for footer_format, method, file_path in self.cases:
            with self.subTest(footer_format):
                context = footer.Context(**yaml.load(contents=self.config))
                self.assertEqual(getattr(footer.Footer(context), method)('updated'),
                                 (resources.files(assets) / file_path).read_text('utf8'))

    def test_footer_env(self):
        """Check the footer with context from the environment."""
        for footer_format, method, file_path in self.cases:
            with (self.subTest(footer_format),
                  mock.patch.dict(os.environ, {'FOOTER_CONFIG': self.config})):
                self.assertEqual(getattr(footer.Footer(), method)('updated'),
                                 (resources.files(assets) / file_path).read_text('utf8'))

    def test_footer_env_file(self):
        """Check the footer with context from a file."""
        for footer_format, method, file_path in self.cases:
            with (self.subTest(footer_format),
                  tempfile.NamedTemporaryFile() as config_file,
                  mock.patch.dict(os.environ, {'FOOTER_CONFIG_PATH': config_file.name})):
                pathlib.Path(config_file.name).write_text(self.config, 'utf8')
                self.assertEqual(getattr(footer.Footer(), method)('updated'),
                                 (resources.files(assets) / file_path).read_text('utf8'))

    def test_footer_main(self):
        """Check the footer main method."""
        for footer_format, _, file_path in self.cases:
            with (self.subTest(footer_format),
                  mock.patch.dict(os.environ, {'FOOTER_CONFIG': self.config})):
                self.assertEqual(footer.main(['--format', footer_format, '--what', 'updated']),
                                 (resources.files(assets) / file_path).read_text('utf8'))

    def test_footer_missing(self):
        """Check the footer behavior for missing configuration."""

        cases = (
            ('minimal', {}, ''),
            ('faq', {'faq_name': 'Foo FAQ', 'faq_url': 'https://faq.url/here'},
             '- Foo FAQ: https://faq.url/here\n'),
            ('slack', {'slack_name': '#foo', 'slack_url': 'https://slack.com/archives/foo'},
             '- Slack #foo: https://slack.com/archives/foo\n'),
            ('source', {'source_code': 'https://source.code/foo.py'},
             '- Source: https://source.code/foo.py\n'),
            ('docs', {'documentation_url': 'https://docs.here/README.foo.md'},
             '- Documentation: https://docs.here/README.foo.md\n'),
            ('issues', {'new_issue_url': 'https://new.issue/new?issue[title]=foo issue'},
             '- Report an issue: https://new.issue/new?issue%5Btitle%5D=foo%20issue\n'),
        )

        config = {'name': 'foo'}
        expected = ('\n----------------------------------------\n\n'
                    'Created 2021-01-01 00:00 UTC by foo:\n')
        for description, additional_config, additional_expected in cases:
            with self.subTest(description):
                config.update(additional_config)
                expected += additional_expected
                self.assertEqual(footer.Footer(footer.Context(**config)).text_footer(), expected)
