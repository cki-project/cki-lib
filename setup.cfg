[metadata]
name = cki-lib
description = Random pieces of code used by the CKI team.
long_description = file: README.md
version = 0.0.1
author = CKI Team
author_email = cki-project@redhat.com
license = GPLv3

[options]
# Automatically find all packages
packages = find:
# Parse the MANIFEST.in file and include those files, too.
include_package_data = True
python_requires = >=3.10
install_requires =
    urllib3
    boto3
    gql
    jsonschema[format]
    kcidb-io @ git+https://gitlab.com/cki-project/mirror/kcidb-io.git@production
    kubernetes
    pika
    prometheus-client
    python-dateutil
    python-gitlab
    PyYAML
    requests
    sentry-sdk
scripts =
    shell-scripts/cki_check_links.sh
    shell-scripts/cki_entrypoint.sh
    shell-scripts/cki_lint.sh
    shell-scripts/cki_test.sh
    shell-scripts/cki_utils.sh

[options.entry_points]
console_scripts =
    cki_is_production = cki_lib.misc:_is_production_cli
    cki_is_staging = cki_lib.misc:_is_staging_cli
    cki_deployment_environment = cki_lib.misc:_deployment_environment_cli

[options.extras_require]
crypto =
    cryptography
dev =
    crontab
    freezegun
    responses
    types-PyYAML
    types-requests
test =
    coverage
    pytest
    pytest-subtests
lint =
    autopep8
    flake8
    isort[colors]
    pydocstyle
    pylint
    ruff
mysql =
    mysql-connector-python
psql =
    psycopg2
remote_responses =
    responses
umb =
    stomp.py

[options.packages.find]
exclude =
    tests*
    inttests*

[flake8]
per-file-ignores =
    cki_lib/kcidb/__init__.py:F401

[tox:tox]
envlist = full

[testenv]
commands = echo "Choose a proper testenv, this is only here to enable pass_env"

[testenv:full]
extras =
    crypto
    dev
    kcidb
    lint
    mysql
    psql
    test
    umb
commands = cki_lint.sh cki_lib
           cki_test.sh cki_lib

[testenv:test]
extras = {[testenv:full]extras}
commands = cki_test.sh cki_lib

[testenv:lint]
extras = {[testenv:full]extras}
commands = cki_lint.sh cki_lib

[testenv:inttests]
extras =
    crypto
    dev
    mysql
    test
    umb
    test
set_env =
    CKI_COVERAGE_CHECK_ENABLED=false
    CKI_PYTEST_IGNORELIST=tests/ inttests/images/
commands = cki_test.sh cki_lib

[testenv:image]
extras =
    dev
    test
set_env =
    CKI_COVERAGE_ENABLED=false
    CKI_PYTEST_ARGS=--verbose -r s inttests/images/{env:IMAGE_NAME}
commands = cki_test.sh cki_lib

[testenv:pipeline]
extras =
    dev
    test
set_env =
    CKI_COVERAGE_CHECK_ENABLED=false
    CKI_PYLINT_ARGS=--disable E0401
install_command = python -I -m pip install --constraint constraints.txt {opts} {packages}
commands = cki_test.sh cki_lib

[coverage:report]
exclude_lines =
    if typing.TYPE_CHECKING:
    if __name__ == .__main__.:
    pragma: no cover
