# Specification of CKI's extension of KCIDB schema over its `misc` fields
---
$defs:
  object-actions: &object-actions
    title: actions
    description: Data about actions performed on the object.
    type: object
    properties:
      last_triaged_at:
        description: Timestamp when this object was last triaged.
        type: ["string", "null"]
        format: date-time
    additionalProperties: false

  provenance-data: &provenance-data
    title: provenance-data
    description: |
      Extra information describing where, when and how the object was generated.
    type: array
    items:
      description: Component involved on the generation of the object.
      properties:
        function:
          description: Responsibility of the component.
          type: string
          enum: [coordinator, provisioner, executor]
        url:
          description: URL to the component.
          type: string
          format: uri
        service_name:
          description: |
            Name of the service that generated the component. Known services are
            'gitlab', 'jenkins' and 'beaker', but any value is accepted.
          type: string
        misc:
          description: |
            Additional information from the service that generated the component.
            This information is not standardized.
          type: object
      required:
        - function
        - url
      additionalProperties: false

  string-or-list-of-strings: &string-or-list-of-strings
    anyOf:
      - type: string
      - type: array
        items:
          type: string

  kcidb-version: &kcidb-version
    description: >
      The version of KCIDB schema used to serialize an object. This field is parsed
      by datawarehouse-kcidb-forwarder to define the canonical version field at
      the root of the payload sent to KCIDB upstream.
    type: object
    properties:
      version:
        type: object
        properties:
          major:
            type: integer
          minor:
            type: integer

  merge-request: &merge-request
    description: Data about the Merge Request that generated the checkout
    type: object
    properties:
      id:
        description: Merge request ID.
        type: integer
      url:
        description: Merge request URL.
        type: string
      diff_url:
        description: Merge request diff URL.
        type: string
    required:
      - url
    additionalProperties: false

  checkout: &checkout
    title: checkout
    description: CKI extension for KCIDB Checkout.
    type: object
    properties:
      misc:
        description: Miscellaneous extra data about the checkout
        type: object
        properties:
          iid:
            description: Integer ID for KCIDBCheckout within Datawarehouse
            type: integer
          is_missing_triage:
            description: >
              Whether the object represents a failure and is waiting to be tagged with a known issue.
            type: boolean
          is_public:
            description: >
              Whether the object can be accessed by anyone without authentication
              and its data should be forwarded to upstream KCIDB.
            type: boolean
          kcidb: *kcidb-version
          retrigger:
            description: >
              True if this is a retriggered checkout and should not result in
              user-visible effects.
            type: boolean
          notification_sent_build_setups_finished:
            description: Whether the "build_setups_finished" notification has been sent
            type: boolean
          notification_sent_tests_finished:
            description: Whether the "tests_finished" notification has been sent
            type: boolean
          ready_to_report:
            description: Whether the "ready_to_report" notification has been sent
            type: boolean
          scratch:
            description: >
              True if the checkout originates either from a Brew/Koji scratch
              build or a merge request.
            type: boolean
          actions: *object-actions
          provenance: *provenance-data
          patchset_modified_files:
            description: >
              Files modified by the patches applied to generate the checkout
            type: array
            items:
              description: Details of the file.
              type: object
              properties:
                path:
                  description: Path of the modified file
                  type: string
              required:
                - path
              additionalProperties: false
          affected_subsystems:
            description: >
              Subsystems affected by the patches applied to generate the checkout
            type: array
            items:
              description: Details of an affected subsystem
              type: object
              properties:
                name:
                  description: subsystem name
                  type: string
                label:
                  description: associated subsystem label used on merge requests
                  type: string
              required:
                - name
                - label
              additionalProperties: false
          brew_task_id:
            description: ID of the Brew or Koji task that generated this checkout
            type: integer
          source_package_name:
            description: Name of the SRPM that generated this checkout
            type: string
          source_package_version:
            description: Version of the SRPM that generated this checkout
            type: string
          source_package_release:
            description: Release of the SRPM that generated this checkout
            type: string
          submitter:
            description: Submitter of the checkout
            type: string
          mr: *merge-request
          related_merge_request: *merge-request
          send_ready_for_test_pre:  # TODO: remove after 2023-12-31
            description: obsolete
            type: boolean
          send_ready_for_test_post:  # TODO: remove after 2023-12-31
            description: obsolete
            type: boolean
          public:  # TODO: remove after 2024-01-31
            description: Publicity of the kernel checkout
            type: [boolean, string]
          kernel_version:
            description: Version of the kernel checkout
            type: string
          all_sources_targeted:
            description: All source files have targeted tests
            type: boolean
          report_rules:
            description: |
              List of rules determining the conditions for reporting this checkout.
              The list should be serialized and have the following properties:
                when:
                  description: Condition when this rule is evaluated
                  type: string
                send_to:
                  description: Recipients or group of them to add as TO
                  <<: *string-or-list-of-strings
                send_bcc:
                  description: Recipients or group of them to add as BCC
                  <<: *string-or-list-of-strings
                report_template:
                  description: >
                    Template to use when generating the report.
                    Defaults to "short" if missing.
                  type: string
                  enum: [short, long, failure-only]
              required:
                - when
              anyOf:
                - required: [send_to]
                - required: [send_bcc]
            type: string
            pattern: ^\[\s*(?P<rule>{(?P<prop>"(?P<key>when|send_to|send_bcc|report_template)":\s*(?P<array_or_string>\[[^]]+\]|"[^"]+")(?:,\s*)?)+}(?:,\s*)?)*\s*\]
        required: []
        additionalProperties: true
    required: []
    additionalProperties: true

  build: &build
    title: build
    description: CKI extension for KCIDB Build.
    type: object
    properties:
      misc:
        description: Miscellaneous extra data about the build
        type: object
        properties:
          iid:
            description: Integer ID for KCIDBBuild within Datawarehouse
            type: integer
          is_missing_triage:
            description: >
              Whether the object represents a failure and is waiting to be tagged with a known issue.
            type: boolean
          is_public:
            description: >
              Whether the object can be accessed by anyone without authentication
              and its data should be forwarded to upstream KCIDB.
            type: boolean
          kcidb: *kcidb-version
          revision_id:
            description: obsolete
            type: string
          debug:
            description: True if this is a debug kernel build.
            type: boolean
          actions: *object-actions
          provenance: *provenance-data
          test_plan_missing:
            description: >
              True if we are expecting to test this build but the plan was
              not generated yet. Needs to be set to False once test plan
              objects are added to the file.
            type: boolean
          package_name:
            description: Name of the kernel package
            type: string
          package_version:
            description: Version of the kernel RPM
            type: string
          package_release:
            description: Release of the kernel RPM
            type: string
          kpet_tree_name:
            description: KPET tree name
            type: string
          testing_skipped_reason:
            description: Reason for not running tests
            type: string
          retrigger:
            description: (deprecated use checkout.retrigger instead)
            type: boolean
        required: []
        additionalProperties: true
    required: []
    additionalProperties: true

  test-result: &test-result
    description: Test run partial result.
    type: object
    properties:
      id:
        description: Result ID.
        type: string
      name:  # TODO: remove after 2024-06-30
        description: obsolete
        type: string
      status:
        description: Result status.
        type: string
        enum: [FAIL, ERROR, MISS, PASS, DONE, SKIP]
      output_files:
        description: Result output files.
        type: array
        items:
          description: Named remote resource.
          type: object
          properties:
            name:
              description: Resource name.
              type: string
            url:
              description: Resource URL.
              type: string
              format: uri
          required:
            - name
            - url
          additionalProperties: false
      build_id:  # TODO: remove after 2024-06-30
        description: obsolete
        type: string
      comment:
        description: Test result name.
        type: string
      misc:
        type: object
        properties:
          iid:
            description: Integer ID for KCIDBTestResult within Datawarehouse
            type: integer
    required:
      - id
      # TODO comment:
      - status
    additionalProperties: false

  test: &test
    title: test
    description: CKI extension for KCIDB Test.
    type: object
    properties:
      misc:
        description: Miscellaneous extra data about the test.
        type: object
        properties:
          iid:
            description: Integer ID for KCIDBTest within Datawarehouse
            type: integer
          is_missing_triage:
            description: >
              Whether the object represents a failure and is waiting to be tagged with a known issue.
            type: boolean
          is_public:
            description: >
              Whether the object can be accessed by anyone without authentication
              and its data should be forwarded to upstream KCIDB.
            type: boolean
          kcidb: *kcidb-version
          beaker:
            description: Information about the beaker task where this test ran.
            type: object
            properties:
              finish_time:
                description: Timestamp when this test finished running.
                type: string
                format: date-time
              recipe_id:
                description: Beaker Recipe id.
                type: integer
              retcode:
                description: Test retcode (deprecated).
                type: integer
              task_id:
                description: Beaker Task ID.
                type: integer
            required:
              - task_id
              - recipe_id
            additionalProperties: false
          debug:
            description: True if this is test is for debug kernel.
            type: boolean
          fetch_url:
            description: URL where the test definition can be fetched from.
            type: string
          group:
            description: >
              information about the test group a test belongs to, e.g. a Beaker
              recipe
            type: object
            properties:
              id:
                description: unique ID for the test group
                type: string
              description:
                description: description for the test group
                type: string
          maintainers:
            description: List of test maintainers.
            type: array
            items:
              description: Details of the maintainer.
              type: object
              properties:
                email:
                  description: Email address.
                  type: string
                  format: email
                gitlab:
                  description: Gitlab username.
                  type: string
                name:
                  description: Full name.
                  type: string
              required:
                - email
                - name
          targeted:
            description: True if this is a targeted test.
            type: boolean
          rerun_index:
            description: >
              Index of test (re)execution, to distinguish between reruns.
              Defaults to 1 if missing.
            type: integer
          forced_skip_status:
            description: >
              True if this test did not run and was forcefully set to SKIP.
              Defaults to False if missing.
            type: boolean
          actions: *object-actions
          retrigger:
            description: (deprecated use checkout.retrigger instead)
            type: boolean
          polarion_id:
            description: Polarion test ID.
            type: string
            format: uuid
          provenance: *provenance-data
          results:
            description: List of test results.
            type: array
            items: *test-result
        required: []
        additionalProperties: true
    required: []
    additionalProperties: true

title: Schema for CKI extensions of KCIDB
description: KCIDB Schema with CKI data
properties:
  checkouts:
    description: List of source code checkouts
    type: array
    items: *checkout
  builds:
    description: List of builds
    type: array
    items: *build
  tests:
    description: List of test runs
    type: array
    items: *test
required: []
additionalProperties: true
