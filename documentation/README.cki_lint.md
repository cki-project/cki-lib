---
title: cki_lint.sh
description: >-
    Wrapper to run a set of common linters across all of CKI
---

Runs `flake8`, `pydocstyle`, `isort --check` and `ruff`. The
script takes care to install the needed dependencies via pip beforehand. For
`pylint`, only the packages specified as parameters will be checked.

Performs linting and link checking for all `README.*.md` files. It uses the
markdownlint tool to check for markdown errors and a `cki_check_links.sh`
script to check for broken links.

The experimental `--fix` parameter can be used to automatically fix some of the
detected issues. At the moment, this will invoke `autopep8` and `isort` without
`--check`.

For correct editor integration of linters/fixers, the following parameters must
be set:

- `flake8`: `--max-line-length 100`
- `autopep8`: `--max-line-length 100`
- `isort`: `--line-length 100 --force-single-line-imports --force-sort-within-sections`

When run in a GitLab CI/CD pipeline for a merge request, the script tries to
determine the previous code coverage and will fail if code coverage decreased.
This can be skipped by adding `[skip coverage check]` with an explanation to
the merge request description.

## Environment variables

| Environment variable         | Description                                                       |
|------------------------------|-------------------------------------------------------------------|
| `CKI_SKIP_PREPARE`           | `true` if Python package installation should be skipped           |
| `CKI_DISABLED_LINTERS`       | linters to disable or `all`                                       |
| `CKI_PYLINT_ARGS`            | Additional arguments for pylint, e.g. to disable certain warnings |

{{% alert title="NOTE" color="info" %}}

`CKI_SKIP_PREPARE=true` can be useful to run checks without network.
When using `tox`, you should probably call: `tox --skip-pkg-install`;
when using `podman`, you should probably call `podman run --net none ...args...`.

{{% /alert %}}

## Running Python tests and linting

To run both Python tests and linting, execute the following command:

```shell
tox
```

To run only the linting use the `lint` tox environment:

```shell
tox -e lint
```

### Disabling linters

To skip certain linters, specify them in the `CKI_DISABLED_LINTERS` variable as a
space-separated list. For example:

```shell
TOX_OVERRIDE=testenv.passenv+=CKI_DISABLED_LINTERS \
    CKI_DISABLED_LINTERS="markdownlint pylint" \
    tox -e lint
```

The above command skips the `markdownlint` and `pylint` linters when running
tox.
