---
title: cki_lib.gitlab
linkTitle: gitlab
description: Access the GitLab GraphQL API via command line
---

Access the GitLab [GraphQL API]. Results are printed as JSON.

```bash
usage: python3 -m cki_lib.gitlab
                 [--gitlab-url GITLAB_URL] --graphql-query GRAPHQL_QUERY
                 [--variables [KEY=VALUE ...]]

options:
  -h, --help            show this help message and exit
  --gitlab-url GITLAB_URL
                        GitLab URL, defaults to GITLAB_URL environment variable
  --private-token PRIVATE_TOKEN
                        GitLab access token, by default derived from GITLAB_TOKENS
  --graphql-query GRAPHQL_QUERY
                        GraphQL query
  --variables [KEY=VALUE ...]
                        JSON-formatted variable values
```

## Configuration via environment variables

| Name            | Type   | Secret | Required | Description                                                           |
|-----------------|--------|--------|----------|-----------------------------------------------------------------------|
| `GITLAB_URL`    | URL    | no     | no       | URL of the GitLab instance                                            |
| `GITLAB_TOKENS` | dict   | no     | no       | URL/environment variable pairs of GitLab instances and private tokens |
| `GITLAB_TOKEN`  | string | yes    | no       | GitLab private tokens as configured in `GITLAB_TOKENS`                |
| `SENTRY_DSN`    | url    | yes    | no       | Sentry DSN                                                            |

## Example

```console
$ python3 -m cki_lib.gitlab \
  --gitlab-url https://gitlab.com \
  --graphql-query '
    mutation ($id: NoteableID!, $body: String!) {
      createNote(input: { noteableId: $id, body:$body, internal: true }) {
        note {
          id
          discussion {
            id
          }
        }
      }
    }' \
  --variables id=gid://gitlab/MergeRequest/12345 \
  --variables 'body="Hello World"'
{"createNote": {"note": {"id": "gid://gitlab/Note/67890", "discussion": {"id": "gid://gitlab/Discussion/0123456789abcdef"}}}}
```

[GraphQL API]: https://docs.gitlab.com/ee/api/graphql/reference/index.html
