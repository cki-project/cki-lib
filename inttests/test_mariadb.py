"""Tests for MariaDB inttest provider."""
import os

import mysql.connector

from cki_lib import mariadb
from cki_lib.inttests import cluster
from cki_lib.inttests.mariadb import MariaDBServer


@cluster.skip_without_requirements()
class TestMariaDB(MariaDBServer):
    """Tests for MariaDB inttest provider."""

    def test_sql(self) -> None:
        """Test accessing the SQL server."""

        connection = mysql.connector.connect(
            host=os.environ['MARIADB_HOST'],
            port=int(os.environ['MARIADB_PORT']),
            user=os.environ['MARIADB_USER'],
            password=os.environ['MARIADB_PASSWORD'],
            database=os.environ['MARIADB_DATABASE'],
            collation='utf8mb4_general_ci',
        )
        cursor = connection.cursor()
        cursor.execute("SHOW DATABASES;")
        databases = cursor.fetchall()
        self.assertIn(os.environ['MARIADB_DATABASE'], {d[0] for d in databases})

    def test_helper(self) -> None:
        """Test the MariaDB helper."""

        handler = mariadb.MariaDBHandler()
        handler.execute('DROP TABLE IF EXISTS example;')
        handler.execute('CREATE TABLE example (id1 int, id2 int);')
        handler.execute('INSERT INTO example VALUES (1, 2);')
        handler.execute('INSERT INTO example VALUES (3, 4);')

        cases = (
            ('equal', '=', '1', []),
            ('equal subst', '=', '%s', [1]),
            ('tuple single', 'IN', handler.tuple_placeholder([1]), [1]),
            ('tuple multiple', 'IN', handler.tuple_placeholder([1, 2]), [1, 2]),
        )

        for description, operator, placeholder, values in cases:
            with self.subTest(description):
                self.assertEqual(handler.execute(
                    f'SELECT * FROM example WHERE id1 {operator} {placeholder};', values,
                ), [(1, 2)])
