"""Tests for Minio inttest provider."""
import os

import boto3

from cki_lib.inttests import cluster
from cki_lib.inttests.minio import MinioServer


@cluster.skip_without_requirements()
class TestMinio(MinioServer):
    """Tests for minio inttest provider."""

    def test_minio(self) -> None:
        """Test basic access to the API."""
        bucket = 'test-bucket'
        key = 'file.txt'
        body = b'file contents'

        client = boto3.session.Session().client('s3', endpoint_url=os.environ['AWS_ENDPOINT_URL'])
        client.create_bucket(Bucket=bucket)
        client.put_object(Bucket=bucket, Key=key, Body=body)
        self.assertEqual(client.get_object(Bucket=bucket, Key=key)['Body'].read(), body)
